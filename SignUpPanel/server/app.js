/**
 * Created by Fara Aileen on 17/10/2016.
 */
var express = require("express");
var bodyParser=require('body-parser');

var app = express();

var NODE_PORT = process.env.PORT || 3000;

app.use("/bower_components", express.static(__dirname + "/../bower_components"));
app.use(express.static(__dirname + "/../client/"));
app.use(bodyParser());



app.listen(NODE_PORT, function(){
    console.log("Server running at http://localhost:"+NODE_PORT);
});
