(function(){
    angular
        .module("SignupApp")
        .controller("SignupCtrl", SignupCtrl);
    
    SignupCtrl.$inject = [];
    function SignupCtrl(){
        var vm = this;

        vm.username = "";
        vm.email = "";
        vm.gender = "";
        
        // Triggered when sign-up button clicked
        vm.signup = signup;

        /////
        function signup(){
            console.info("register click");
            console.info("username: %s", vm.username);
            console.info("email: %s", vm.email);
            console.info("gender: %s", vm.gender);
        } // END signUp
    } // End SignUpCtrl
})();