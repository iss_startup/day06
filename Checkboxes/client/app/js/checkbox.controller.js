(function(){
    angular
        .module("CheckboxApp")
        .controller("CheckCtrl", CheckCtrl);

    CheckCtrl.$inject = [];

    console.log("in controller");
    function CheckCtrl(){
        vm = this;
        vm.fruits = {
            apple : true,
            banana : 'YES',
            mango : 'nope'
        };

        vm.submit = submit;

        ///////
        function submit(){
            console.log(vm.fruits);
        }
    } // End CheckCtrl
})();